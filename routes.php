<?php

$router->get('', 'HomeController@index');
$router->get('addresses', 'AddressesController@list');
$router->get('addresses/new', 'AddressesController@new');
$router->get('addresses/modify', 'AddressesController@modify');
$router->get('addresses/searchPostalCode', 'AddressesController@searchPostalCode');
$router->get('addresses/form', 'AddressesController@formView');

$router->post('addresses/create', 'AddressesController@create');
$router->post('addresses/update', 'AddressesController@update');
$router->post('addresses/delete', 'AddressesController@delete');
$router->post('addresses/form', 'AddressesController@form');
$router->get('addresses/update', 'AddressesController@update');
