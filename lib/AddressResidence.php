<?php

/**
 * Állandó címkezelő bővítés
 */

class AddressResidence extends Address {

	// property redeclare
	public $country_name = "Scotland";

	/**
	 * method override
	 * Több vagy kevesebb paramétere nem lehet, mint az eredeti!
	 */
	public function display()
	{
		$output = '<div class="alert alert-info residence">
		' . parent::display() . '
		</div>';

		return $output;
	}

	/**
	 * Inicializálás a címptípus beállítására
	 * @return [type] [description]
	 */
	protected function _init()
	{
		$this->_setAddressTypeId(parent::ADDRESS_TYPE_RESIDENCE);
	}
}