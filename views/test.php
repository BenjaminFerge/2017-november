<?php


echo '<h2>Objektum készítése asszociatív tömbből a konstruktor használatával</h2>';
$data = [
	'address_line_1' => 'teszt utca 2.',
	'city_name' => 'Törökbálint',
	// 'country_name' => 'Magyarország',
	// 'address_type_id' => 1,
	// 'postal_code' => 5555
];

echo '<h2>Osztály bővítések</h2>';

echo '<h5>Residence</h5>';
$address_residence = new AddressResidence($data);
echo '<pre>' . var_export($address_residence, true) . '</pre>';
echo $address_residence->display();

echo '<h5>Business</h5>';
$address_business = new AddressBusiness($data);
echo '<pre>' . var_export($address_business, true) . '</pre>';
echo $address_business->display();

echo '<h5>Temporary</h5>';
// $data['address_type_id'] = 1;
$address_temporary = new AddressTemporary($data);
echo '<pre>' . var_export($address_temporary, true) . '</pre>';
echo $address_temporary->display();
try {
	$load_id = 3;
	echo "<h5>Cím betöltése adatbázisból (id: $load_id)</h5>";
	$address_db = Address::load($load_id);
	echo '<pre>' . var_export($address_db, true) . '</pre>';
	echo '<h5>Módosítás után</h5>';
	$address_db->address_line_2 = 'Teszt';
	$address_db->update();
	$address_db = Address::load($load_id);
	echo '<pre>' . var_export($address_db, true) . '</pre>';

	echo '<h5>Törlés után</h5>';
	$address_db->delete();
	$address_db = Address::load($load_id);
	echo '<pre>' . var_export($address_db, true) . '</pre>';
}
catch (Exception $e) {
	echo $e;
}
// $address_to_save = Address::getInstance(1, $data);
// $inserted_id = $address_to_save->save();
// if ($inserted_id) {
// 	echo '<div class="alert alert-success">Objektum sikeresen elmentve. Azonosítója: <strong>' . $inserted_id . '</strong></div>';	
// }
// echo '<pre>' . var_export($address_to_save, true) . '</pre>';