<!DOCTYPE html>
<html>
<head>
    <title>Címkezelő osztály</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <style type="text/css">
    	html, body {
		  height: 100%;
		}
		body {
		  display: flex;
		  flex-direction: column;
		}
		.content {
		  flex: 1 0 auto;
		}
		
    </style>

</head>
<body>

<div class="container content">

    <?php include('nav.php'); ?>
