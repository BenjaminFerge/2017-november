﻿git commit -am "tag" : az új fájlakat nem rakja bele, arra ott a "git add"!!!
bash paste: (shift / ctrl) + ins

git push -u (upstream) origin (hova) master (mit)

git push : ha egy van

gépen lévőek listák:
git remote -v : távoli lista
git branch -v : branch lista

git checkout "branch-név"
váltani csak akkor lehet, ha a "working tree is clean"

_______________
oop php

osztály CamelCase javallott, számmal nem kezdődhet, kötőjel operátor...


protected, private változók _ kezdődnek általában


__get /set akkor fut, ha nem létező, vagy védett propertyt érünk el

	public function __get($name) {
		// trigger_error ("Fut a get: $name");
	}


	public function __set($name, $value) {
		// trigger_error ("Fut a set: $name => $value");
	}
	
HF:
Kerületekre osztott városok nélkül (Debrecen, BP) lista irányítószámmal
~ 3600 row
irsz | város | városrész

______________________________________________________
3. óra

### HF 1:
Melyik a leghosszabb településnév? SQL / PHP
https://posta.hu/szolgaltatasok/iranyitoszam-kereso

- GIT
git clone {repo} {mibe}
git branch -v: branch lista
git remote -v: remote lista
git remote rm origin: remove repo
git remote add {helyi név} {távoli repo}: add repo //origin = eredet
git push -u {hova repo} {branch}: push to repo

Git folytatás, clone, remote váltás, több remote kezelés, fejlesztés folytatása új repoban. Irányítószám xls konvertálása csv-re, importálás jó karakterkódolással sql adatbázisba.

Virtual Server on Windows
system32/drivers/etc/hosts módosítása


apache vhosts.conf

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs"
    ServerName localhost
    ErrorLog "logs/localhost.oop-error.log"
    CustomLog "logs/localhost-access.log" common
</VirtualHost>


<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs/fb-oop"
    ServerName fb.oop
    ErrorLog "logs/fb.oop-error.log"
    CustomLog "logs/fb.oop-access.log" common
</VirtualHost>

localhost legyen az első irányítás


OOP
echo {array} => "array"

Object-nél van magic method: __toString()


Destruktor:
akkor fut, ha már nem lesz több hívása.
pl.: mysqli kapcsolat close

### HF 2:
Űrlap elkészítése

lenyíló: címtípus
alatta rövidebb irányítószám mező, readonly
mellette városnév
városrész írható, számít, hogy üres, vagy sem
ajax / újratöltéssel is lehet
hibaüzenetek: 
	nem megfelelő címtípus,
	nem találtam.
kiadható statikus method
külön fájl

Address class (CRUD):
- Create,
- Read,
- Update,
- Delete
Mindegyiknél léteznie kell a példánynak, onnan kell hívni. Publikusnak kell lennie.

### HF 3:
adatbázis terv létrehozása ennek tárolására

Table: addresses
Columns:
address_id int(11) AI PK 
address_line_1 varchar(45) 
address_line_2 varchar(45) 
postal_code varchar(45) 
country_name varchar(45) 
city_name varchar(45) 
subdivision_name varchar(45) 
address_type_id varchar(45) 
time_created timestamp 
time_updated timestamp


_______________________________
4. óra, 2017.11.19.

Minden magic method public
Abstract funkciónál nem írunk logikát, csak megköveteljük létét az osztályából származtatottól (extends)
Abstract funkció megköveteli az abstract osztályt
Abstract osztályt nem példányosíthatunk

__autoload magic method akkor fut, ha egy nem létező osztályt akarunk elérni.
PSR-4 autoloader javasolt komolyabb mappastruktúra esetén.

Abstract metódus soha nem lehet final.

git conflict fájlokban kereshetünk ezekre: <<<< >>>>

PHP Design Patterns:
factory: képes legyártani magát
singleton: egyke, getInstance
strategy:

### HF 4a: Address->update() => kész.

Interface: az interfészt implementált osztály több metódust is tartalmazhat, de a definiáltak kötelezőek.
Felsorolás szinten több is implementálható.
Osztályt bővíteni egyszerre csak egyet, viszont azt akárhányszor.
Véglegesített interfészt kiegészíteni nem illi, az összes osztály, amely implementálja fatális hibára fut. Újat szoktak ráakasztani.

Saját Exception absztrakció ref:
http://php.net/manual/en/class.exception.php

### HF 4b:
- Ha az objektum nem létezik és update-t futtatom, mentse le
- Ha létezik és save-t futtatom update
- all() táblázathoz módosít és töröl gomb

__________________________
óra 5.

Laravel, MVC
peopleperour.com