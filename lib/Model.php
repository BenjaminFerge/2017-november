<?php

// CRUD interface
// Nem azért Model, mert MVC, hanem az adatbázisban, ill. kívülre ő fog foglalatoskodni.
// Nem nevesítjük Address-re, mivel nem csak ott akarjuk majd használni.
interface Model {

	// Model betöltése
	public static function load($id);

	// Model mentése (Create)
	public function save();

	// Model listázása (Read)
	public static function all();

	// Model frissítése (Update)
	public function update();

	// Model törlése (Delete)
	public function delete();

}