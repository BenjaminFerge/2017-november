<?php 

function __autoload($name) {
	$class_name = "lib/{$name}.php";
	if(!file_exists($class_name)) {
		$class_name = "controllers/{$name}.php";
	}
	include $class_name;
}

function dd($var) {
	echo '<pre>';
	var_dump($var);
	die();
}

function dump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}


$router = new Router;
Router::load('routes.php')->direct(Request::uri(), Request::method());


?>

