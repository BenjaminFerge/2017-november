<?php

/*
* Címkezelő osztály
*/

// implements Model

/**
 * Abstract osztályt nem példányosíthatunk!
 * valós példa: jármű, cím
 */
abstract class Address implements Model
{
    // Konstansok a címtípusok azonosítására
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;

    // Hibakódok
    const ADDRESS_ERROR_NOT_FOUND = 1000;
    const ADDRESS_ERROR_INVALID_TYPE = 1001;

    // Osztály példányosítás nélkül elérhető (bár ott is elérhető), osztályszintű címtípusok
    // Address / self is használható
    // Konstans elé nem kell $, ő nem változó
    public static $valid_address_types = [
        self::ADDRESS_TYPE_RESIDENCE => 'Residence',
        Address::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary'
    ];

    public static $protected_variables = [
        '_postal_code',
        '_address_id',
        '_address_type_id',
        '_time_created',
        '_time_updated'
    ];

    // címsor 1
    public $address_line_1;

    // címsor 2
    public $address_line_2;

    // irsz
    protected $_postal_code;

    // ország
    public $country_name;

    // város
    public $city_name;

    // városrész
    public $subdivision_name;

    // cím azonosító
    protected $_address_id;

    // cím típus azonosító
    protected $_address_type_id;

    // timestamps (defaultok beállítása)
    protected $_time_created = 12345;
    protected $_time_updated = 67895;

    /**
     * Konstruktornál lehet védettet beállítani és metódust hívni
     * default értéket lehet itt is adni
     */
    public function __construct($data = [])
    {
        $this->_init();
        $this->_time_created = time();
        // Ha kaptunk adatokat és az tömb, megpróbáljuk felépíteni az objektumot, ha nem jó valami, trigger error
        if (is_array($data)) {
            foreach ($data as $property => $value) {
                if (in_array('_' . $property, self::$protected_variables)) {
                    $property = '_' . $property;
                }
                $this->$property = $value;
            }
            return;
        }

        trigger_error('A kapott adatokból nem építhető fel az objektum.');
    }

    /**
     * getter - akkor fut, amikor nem létező vagy védett tulajdonságot próbálunk kiolvasni
     * @param  string $name
     * @return mixed
     */
    public function __get($name)
    {

        // Ha nem lenne irsz, keressünk...
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }

        $protected_property_name = '_' . $name; // ha van védett tulajdonság, akkor talán erre gondoltál (konvenció védettre: _ (prefix)):
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }

        trigger_error("Védett, vagy nem létező tulajdonságot próbálsz elérni: $name");
    }

    /**
     * setter, mint a getter...
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value)
    {
        // kivét megadása, hogy kívülről lehessen felvinni eddig nem létező postal_code tulajdonságot
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }

        // címtípus átvétele
        if ($name == 'address_type_id') {
            $this->_setAddressTypeId($value);
            return;
        }
        trigger_error("Védett, vagy nem létező tulajdonságot próbálsz beállítani: $name => $value");
    }

    /**
     * Magic method. Objektum echo-nál fut, ha nincs, akkor catchable fatal error.
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->display();
        } catch (Exception $e) {
            return ' ';
        }
    }

    /**
     * _init metódus megkövetelése
     * @return [type] [description]
     */
    abstract protected function _init();

    /**
     * IRSZ keresés város és városrész alapján
     * @todo: write the database logic
     * @return string
     */
    protected function _postal_code_search()
    {
        $db = Database::getInstance();

        // példányosítás
        $mysqli = $db->getConnection();
        $query = "SELECT irsz FROM telepulesek";
        // kapott adatok escape-elése.
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);

        $query .= "
		WHERE telepules_nev = '$city_name' 
		AND telepules_resz = '$subdivision_name'
			#limit 0"; // sql comment: #

        // lekérés
        $result = $mysqli->query($query);
        // kibontás
        $row = $result->fetch_object();

        if ($row) {
            return $row->irsz;
        }

        // var_dump($db);die('');
        http_response_code(404);
        // return 'Nem találtam!';
        return null;
    }


    /**
     * Objektum kiíratása
     * @return string
     */
    public function display()
    {

        // címsor 1
        $output = $this->address_line_1;
        // ha van, címsor 2
        if ($this->address_line_2) {
            $output .= '<br>' . $this->address_line_2;
        }
        //város, városrész ha van
        $output .= '<br>' . " {$this->city_name} " . ($this->subdivision_name ? ", $this->subdivision_name" : "");

        $output .= '<br>' . $this->country_name;
        $output .= "<br>$this->postal_code";

        return $output;

    }

    public static function all()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $query = "SELECT * FROM addresses";
        $result = $mysqli->query($query);

        if ($result->num_rows > 0) {
            $data = [];
            while ($row = $result->fetch_object()) {
                array_push($data, $row);
            }
            return $data;
        } else {
            return null;
        }
    }


    /**
     * Címtípus azonosító ellenőrzése - akár kívülről is, példány nélkül hívható
     * @param  int $address_type_id
     * @return boolean
     */
    public static function isValidAddressTypeId(int $address_type_id)
    {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Címtípus azonosító védett tulajdonság beállítása
     * @param int $address_type_id
     */
    protected function _setAddressTypeId($address_type_id)
    {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
            return;
        }

        trigger_error("Nem érvényes címtípus azonosító!");
    }


    /**
     * Address osztály dinamikus példányosítása
     * Abstract osztályból csak így tudunk példányosítani
     */
    public static function getInstance($address_type_id, $data = [])
    {
        if (!self::isValidAddressTypeId($address_type_id)) {
            throw new ExceptionAddress("Érvénytelen címtípus, nem lehet példányosítani.<br>address_id: " . $data['address_id'] . "<br>address_type_id: $address_type_id", self::ADDRESS_ERROR_INVALID_TYPE);
        }
        $class_name = 'Address' . self::$valid_address_types[$address_type_id];
        $reflector = new ReflectionClass($class_name);
        // if ($reflector->isAbstract()) {
        // 	throw new ExceptionAddress("Érvénytelen címtípus, nem lehet példányosítani.<br>address_id: ".$data['address_id']."<br>address_type_id: $address_type_id", self::ADDRESS_ERROR_INVALID_TYPE);
        // }
        return new $class_name($data);
    }

    // Interface metódusok
    // Cím betöltése
    public final static function load($id)
    {
        $query = '
		SELECT * FROM addresses 
		WHERE address_id = ' . (int)$id . '
		LIMIT 1';
        $db = Database::getInstance();

        $mysqli = $db->getConnection();

        $result = $mysqli->query($query);

        $address = null;
        if ($row = $result->fetch_assoc()) {
            $address = self::getInstance($row['address_type_id'], $row);
            return $address;
        }

        throw new ExceptionAddress("Nem sikerült betölteni: " . $id, self::ADDRESS_ERROR_NOT_FOUND);

        return;
    }

    /**
     * Cím mentése
     * @return [type] [description]
     */
    public final function save()
    {
        $address = Address::getInstance($this->address_type_id, $_POST);
        if ($address->_address_id) {
            $this->update();
            return;
        }

        $db = Database::getInstance();

        $mysqli = $db->getConnection();
        $query = "INSERT INTO addresses (
		`country_name`,
		`address_line_1`,
		`address_line_2`,
		`postal_code`,
		`city_name`,
		`subdivision_name`,
		`address_type_id`,
		`time_created`) VALUES (
			'{$mysqli->real_escape_string($this->country_name)}',
			'{$mysqli->real_escape_string($this->address_line_1)}',
			'{$mysqli->real_escape_string($this->address_line_2)}',
			'{$mysqli->real_escape_string($this->postal_code)}',
			'{$mysqli->real_escape_string($this->city_name)}',
			'{$mysqli->real_escape_string($this->subdivision_name)}',
			'{$mysqli->real_escape_string($this->address_type_id)}',
			'" . date('Y-m-d H:i:s') . "'
		)";

        if (!$this->postal_code) {
            throw new ExceptionAddress("A megadott településhez nem találtam irányítószámot! ({$this->city_name})", self::ADDRESS_ERROR_NOT_FOUND);
        }
        $result = $mysqli->query($query) or die('Hiba: ' . $query . '<br>' . $mysqli->error);

        if ($mysqli->insert_id) {
            $this->_address_id = $mysqli->insert_id;
        }

        return $mysqli->insert_id;
    }

    /**
     * Cím törlése
     * @param  [type] $address_id [description]
     * @return [type]             [description]
     */
    public final function delete()
    {

        $db = Database::getInstance();

        $mysqli = $db->getConnection();

        $query = "DELETE FROM addresses WHERE address_id = {$this->_address_id}";

        $result = $mysqli->query($query);

        if ($result === true) {
            return true;
        } else {
            throw new Exception("Hiba: " . $mysqli->error);
        }
    }

    /**
     * Cím frissítése
     * @return [type] [description]
     */
    public final function update()
    {
        $address = Address::getInstance($this->address_type_id, $_POST);
        if (!$address->_address_id) {
            $this->save();
            return;
        }
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $query = "
		UPDATE addresses SET 
			`country_name` = '{$mysqli->real_escape_string($this->country_name)}',
			`address_line_1` = '{$mysqli->real_escape_string($this->address_line_1)}',
			`address_line_2` = '{$mysqli->real_escape_string($this->address_line_2)}',
			`postal_code` = '{$mysqli->real_escape_string($this->postal_code)}',
			`city_name` = '{$mysqli->real_escape_string($this->city_name)}',
			`subdivision_name` = '{$mysqli->real_escape_string($this->subdivision_name)}',
			`address_type_id` = '{$mysqli->real_escape_string($this->address_type_id)}',
			`time_updated` = '" . date('Y-m-d H:m:i') . "' 
		WHERE address_id = '{$mysqli->real_escape_string($this->address_id)}'";

        $result = $mysqli->query($query) or die('Hiba: ' . $query . '<br>' . $mysqli->error);

        return;
    }

    public function getAddressTypeId()
    {
        return $this->_address_type_id;
    }

}