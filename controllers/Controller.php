<?php
abstract class Controller {

	public function view($page, $data = []) {
		extract($data);
		
		require 'views/partials/header.php';
		require "views/{$page}.php";
		require 'views/partials/footer.php';
	}

	public static function error($errorNumber){
		http_response_code($errorNumber);
		require 'views/partials/header.php';
		require "views/error.php";
		require 'views/partials/footer.php';
	}
}