
</div>

<footer class="footer">
  <div class="container">
    Ruander &copy <?php echo date("Y"); ?> | Ferge Benjámin
  </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>



<script>
$('.delete').click(function(){
	// var address_id = $(this).closest('tr').children('td').first().html();

	var address_id = $(this).attr('address-id');
	if(!confirm("Biztosan törli a kijelölt címet? ("+address_id+")")) {
		return;
	}
	var data = new FormData();
	data.append('address_id', address_id);
	axios.post('/addresses/delete', data)
	.then(function (response) {
		location.reload();
	})
	.catch(function (error) {
		console.log(error);
	});
})

$('#search').click(function(){
  var city_name = $("#city_name").val();
  var subdivision_name = $("#subdivision_name").val();
  axios.get('/addresses/searchPostalCode', {
  		params: {
  			city_name: city_name,
  			subdivision_name: subdivision_name
  		}
  	})
    .then(function (response) {
      $('#postal_code').val(response.data);
    })
    .catch(function (error) {
      alert("Irányítószám nem található!");
    });
})

</script>

</body>
</html>