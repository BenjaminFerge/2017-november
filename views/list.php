<h1>Címlista</h1>
<table class="table table-striped">
	<tr>
		<th>Azonosító</th>
		<th>Cím 1</th>
		<th>Cím 2</th>
		<th>IRSZ</th>
		<th>Ország</th>
		<th>Város</th>
		<th>Megye</th>
		<th>Címtípus</th>
		<th>Létrehozva</th>
		<th>Módosítva</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
<?php foreach (Address::all() ?? [] as $address): ?>
	<tr>
		<td><?=$address->address_id?></td>
		<td><?=$address->address_line_1?></td>
		<td><?=$address->address_line_2?></td>
		<td><?=$address->postal_code?></td>
		<td><?=$address->country_name?></td>
		<td><?=$address->city_name?></td>
		<td><?=$address->subdivision_name?></td>
		<td><?=Address::$valid_address_types[$address->address_type_id]?></td>
		<td><?=$address->time_created?></td>
		<td><?=$address->time_updated?></td>
        <td><a href="/addresses/modify?id=<?=$address->address_id?>" class="btn btn-sm btn-warning">Módosítás</a></td>
		<td><button address-id="<?=$address->address_id?>" class="delete btn btn-sm btn-danger">Törlés</button></td>
	</tr>
<?php endforeach;?>
</table>

