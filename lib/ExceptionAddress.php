<?php

/**
 * Saját kivételkezelés
 */

class ExceptionAddress extends Exception {
	public function __toString() {
		$msg = __CLASS__ . " [#{$this->code}]: " . $this->getMessage() . " 
		<br>
		{$this->file} [sor: {$this->line}]";
		return $this->format($msg);
	}
	public function format($msg) {
		return '<div class="alert alert-danger">' . $msg . '</div>';
	}
}