<?php

class AddressesController extends Controller
{


    // POST metódusok

    public function create()
    {
        $address_type_id = filter_input(INPUT_POST, 'address_type_id', FILTER_VALIDATE_INT);
        // hf: hackelt address_type_id kezelése az űrlapról
        // ha nincs hiba, az adatokból példányosítható
        if (!filter_input(INPUT_POST, 'postal_code')) {
            unset($_POST['postal_code']);
        }
        $data = $_POST;
        try {
            $address = Address::getInstance($address_type_id, $data);
            $address->save();
            // $address->update();
        } catch (ExceptionAddress $exception) {
            return $this->view('new', ['error' => $exception->getMessage()]);
        }

        header("Location: http://{$_SERVER['HTTP_HOST']}/addresses");
    }

    public function update()
    {
        $address = Address::getInstance($_POST['address_type_id'], $_POST);
        $address->update();
        // $address->save();
        header("Location: http://{$_SERVER['HTTP_HOST']}/addresses");
    }

    public function delete()
    {
        $address_id = $_POST['address_id'] ?? null;
        if (!$address_id) {
            http_response_code(404);
            return;
        }

        $address = Address::load($address_id);
        $address->delete();
        header("Location: http://{$_SERVER['HTTP_HOST']}/addresses");
    }


    // GET metódusok

    public function list()
    {
        return $this->view('list');
    }

    public function new()
    {
        return $this->view('new');
    }

    public function modify()
    {
        $id = filter_input(INPUT_GET, 'id');
        if (!$id) {
	        return $this->list();
        }
        $address = Address::load($id);
        return $this->view('modify', compact('address'));
    }

    public function searchPostalCode()
    {
        $address = Address::getInstance(1, $_GET);
        echo $address->postal_code;
    }

    public function formView()
    {
        return $this->view('form');
    }

    public function form()
    {
        $data = $_POST;
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
        echo '<hr>';
        if (!filter_input(INPUT_POST, 'postal_code')) {
            unset($_POST['postal_code']);
        }

        $address_type_id = filter_input(INPUT_POST, 'address_type_id', FILTER_VALIDATE_INT);
        // hf: hackelt address_type_id kezelése az űrlapról
        // ha nincs hiba, az adatokból példányosítható

        $address = Address::getInstance($address_type_id, $data);
        echo '<pre>';

        var_dump($address);
        echo '</pre>';
        echo '<hr>';
        $address->save();
        echo '<pre>';
        var_dump($address);
        echo '</pre>';
        echo '<hr>';

        return $this->formView();
    }

}