<?php

/**
 * Adatbázis kezelő singleton - egyke, csak egy példány létezhet
 * ha nem létezik megcsinálja és kiadja, ha létezik, akkor csak kiadja
 */

class Database {
	// kapcsolat eltárolása
	private $_connection;

	// példány eltárolása
	// static, mert példányosítás nélkül kell működnie
	private static $_instance;

	/**
	 * Védett példány kiadása.
	 * az $_isntance most nem property, azért kell a $, hanem osztályváltozó
	 */
	public static function getInstance() {
		if (!self::$_instance) {
			// ha nincs, elkészítjük
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	public function __construct()
	{
		//kapcsolat felépítése
		$this->_connection = new mysqli('127.0.0.1', 'homestead', 'secret', 'fb_oop');

		//kódlap beállítása, enélkül ékezeteseket nem talál
		$this->_connection->set_charset('utf8');
		
		// trigger, ha van hiba
		$error = $this->_connection->connect_error;
		if ($error) {
			trigger_error("Nem sikerült az adatbázis csatlakozás: $error", E_USER_ERROR);
		}

	}

	/**
	 * DB kapcsolat kiadása
	 * @return mysqli_connection
	 */
	public function getConnection() {
		return $this->_connection;
	}

	/**
	 * Védelem klónozás ellen.
	 * final: sehogy nem írható felül. (pl. extend sem). próbálkozás esetén fatal error.
	 */
	final public function __clone(){}
}