<?php

class Router {

	protected $routes = [
		'GET' => [],
		'POST' => []
	];


	public function get($uri, $controller) {
		$this->routes['GET'][$uri] = $controller;
	}

	public function post($uri, $controller) {
		$this->routes['POST'][$uri] = $controller;
	}

	public static function load($file) {
		$router = new self;
		require $file;
		return $router;
	}

	public function direct($uri, $method) {
		if (array_key_exists($uri, $this->routes[$method])) {
			return $this->callAction(...explode('@', $this->routes[$method][$uri]));
		}

		return Controller::error(404);
	}

	public function callAction($controller, $action) {
		$controller = new $controller;
		if(!method_exists($controller, $action)) {
			throw new Exception("Nem definiált kérés.");
		}
		return $controller->$action();
	}

}