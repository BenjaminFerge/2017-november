<h1>Módosítás</h1>

<?php
if (isset($error)) {
    echo "<div class='alert alert-danger'>" . $error . "</div>";
}
?>

<form action="/addresses/update" method="post">

    <input type="hidden" value="<?=$address->address_id?>" name="address_id">
    <div class="form-row">
        <div class="form-group col">
            <label for="address_type_id">Címtípus</label>
            <select name="address_type_id" class="form-control" id="address_type_id" required>
                <option value="">Kérem válasszon!</option>
                <?php foreach (Address::$valid_address_types as $address_type_id => $address_type): ?>
                    <option <?= $address->getAddressTypeId() == $address_type_id ? 'selected' : null ?>
                            value="<?= $address_type_id ?>"><?= $address_type ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group col">
            <label for="country_name">Ország</label>
            <input type="text" value="<?= $address->country_name ?>" name="country_name" class="form-control"
                   id="country_name" placeholder="Ország" required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="city_name">Város</label>
            <input type="text" value="<?= $address->city_name ?>" name="city_name" class="form-control" id="city_name"
                   placeholder="Város" required>
        </div>
        <div class="form-group col">
            <label for="subdivision_name">Városrész</label>
            <input type="text" value="<?= $address->subdivision_name ?>" name="subdivision_name" class="form-control"
                   id="subdivision_name" placeholder="Városrész">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group">
            <button id="search" class="btn btn-secondary" type="button">Keresés</button>
        </div>
        <div class="form-group">
            <input type="text" value="<?= $address->postal_code ?>" name="postal_code" class="form-control"
                   id="postal_code" placeholder="IRSZ" readonly required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="address_line_1">Címsor 1</label>
            <input type="text" value="<?= $address->address_line_1 ?>" name="address_line_1" class="form-control"
                   id="address_line_1" placeholder="Cím 1" required>
        </div>
        <div class="form-group col">
            <label for="address_line_2">Címsor 2</label>
            <input type="text" value="<?= $address->address_line_2 ?>" name="address_line_2" class="form-control"
                   id="address_line_2" placeholder="Cím 2">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Mentés</button>
</form>



