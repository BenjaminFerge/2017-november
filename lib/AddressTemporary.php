<?php

/**
 * Állandó címkezelő bővítés
 */

class AddressTemporary extends Address {

	// property redeclare
	public $country_name = "Scotland";

	/**
	 * method override
	 * Több vagy kevesebb paramétere nem lehet, mint az eredeti!
	 */
	public function display()
	{
		$output = '<div class="alert alert-danger temporary">
		' . parent::display() . '
		</div>';

		return $output;
	}

	/**
	 * Inicializálás a címptípus beállítására
	 * @return [type] [description]
	 */
	protected function _init()
	{
		$this->_setAddressTypeId(parent::ADDRESS_TYPE_TEMPORARY);
	}
}